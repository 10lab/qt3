#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtWidgets>
#include <QtGui>
#include <QtCore>
#include <QABSTRACTITEMMODEL.h>
#include <QDebug>
#include <iostream>
#include <fstream>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent *event);
    ~Dialog();
signals:
    void butnpressed(int key);
    void sendDirName(QString DirName);
private slots:
    void on_pushButton_clicked();
    void showImg(QString imgName);
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
